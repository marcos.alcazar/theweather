# TheWeather App

![alt text](https://gitlab.com/marcos.alcazar/theweather/-/raw/master/TheWeather.png "Screenshot")

Welcome to TheWeather app, a simple solution to find out the weather of a given location.
It uses pyowm (https://pyowm.readthedocs.io/) to communicate with OpenWeatherMap API. It
also uses pyowm cache solution to avoid multiple API querys when not needed.

It has being built using:
- Django as backend framework
- djangorestframework to expose an small API for performing searches against to
- pyowm as mentioned, to communicate this app with OpenWeatherMap's API.
- The front-end has been built using Django template languages, jquery and jquery-tmpl to render the results.

###### Install and run it locally
- Get the code at https://gitlab.com/marcos.alcazar/theweather
- Install the dependencies (using a virtual environment is suggested) 
```
pip install -r requirements.txt
```
- Create a `.env.json` file in the root of the project with the following contents (replace with
your own keys)
```
{
  "SECRET_KEY": "your secret key",
  "OWM_API_KEY": "your openweathermaps api key"
}
```
- Run it
```
./manage.py runserver
```
- Open your browser and navigate to `http://localhost:8000`

###### Posible improvements
- Add client-side validation for the query.
- Include forecast to the results (next 3/5 days).
- Switch to a proper front-end framework, the current solution won't scale properly in the future.
- Show in a map
- Better filtering
- User select for units (wind, temp, etc)
- Change the cache for results from the API to a better solution (memcache, redis, etc).
- Use user language from the browser