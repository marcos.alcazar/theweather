Genome Code Challenge
Thank you for your interest in Genome.
Overview
You’ll write a simple weather application to show the current weather in a given location.
We recommend using the Open Weather Map JSON API http://openweathermap.org/API to get
real weather data.
What should the application do?
1. Display the weather information for any given city
2. Cache the fetched weather data (optional)
3. Manage the cases where the weather’s information for a given city can’t be found

What should I do?
1. Create a new Rails project. In our day to day work, we use build web APIs using Ruby. If
you don’t have experience with Ruby, feel free to use the framework you’re more familiar
with.
2. Write your solution having in mind that we embrace best practices and widely accepted
standards, e.g.:
a. Tests (Unit tests, requests specs, integration specs)
b. Code that follows code guidelines, those will vary from language to language
c. Understandable structure
d. Code that makes usage of Object-Oriented Design
e. When it is possible, approach the problem using a functional mindset
3. Create a README.md in the root of the project and provide a project overview. It would
be great if you can point out what you could improve in the project.
4. Push your code to a public repository on (GitHub, GitLab, BitBucket) or any other
repository. If you don’t have one, please create it.
Don’t hesitate to contact us if you have any questions or you need help.