from django.urls import path

from theweather.views import SearchView


app_name = 'theweather'
urlpatterns = [
    path('search/', SearchView.as_view(), name='search'),
]
