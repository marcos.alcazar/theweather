import json
from json import loads
from unittest.mock import patch, MagicMock

from django.test import TestCase, Client


class TestSearch(TestCase):

    def test_query_empty(self):
        c = Client()
        response = c.get('/theweather/search/')
        assert response.status_code == 400

    @patch('pyowm.commons.http_client.requests.get')
    def test_query_nonexisting(self, mocked_post):
        text = '''
            {"cod":"200","count":0,"list":[]}
        '''
        mocked_post.return_value = MagicMock(status_code=200,
                                             json=lambda: loads(text))

        c = Client()
        response = c.get('/theweather/search/', {'q': 'I dont exist'})
        assert response.status_code == 200
        response_json = json.loads(response.content)
        assert isinstance(response_json['weather_info'], list)
        assert len(response_json['weather_info']) == 0

    @patch('pyowm.commons.http_client.requests.get')
    def test_query_by_id(self, mocked_post):
        text = '''
        {"coord":{"lon":145.77,"lat":-16.92},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}],"base":"stations","main":{"temp":300.15,"pressure":1007,"humidity":74,"temp_min":300.15,"temp_max":300.15},"visibility":10000,"wind":{"speed":3.6,"deg":160},"clouds":{"all":40},"dt":1485790200,"sys":{"type":1,"id":8166,"message":0.2064,"country":"AU","sunrise":1485720272,"sunset":1485766550},"id":2172797,"name":"Cairns","cod":200}
        '''
        mocked_post.return_value = MagicMock(status_code=200,
                                             json=lambda: loads(text))

        c = Client()
        response = c.get('/theweather/search/', {'q': '2172797'})
        assert response.status_code == 200
        response_json = json.loads(response.content)
        assert isinstance(response_json['weather_info'], list)
        assert len(response_json['weather_info']) > 0
        assert response_json['weather_info'][0].get('location') == "Cairns, AU (145.77, -16.92)"

    @patch('pyowm.commons.http_client.requests.get')
    def test_query_by_cityname(self, mocked_post):
        text = '''
        {"message":"accurate","cod":"200","count":1,"list":[{"id":2643743,"name":"London","coord":{"lat":51.5085,"lon":-0.1258},"main":{"temp":280.15,"pressure":1012,"humidity":81,"temp_min":278.15,"temp_max":281.15},"dt":1485791400,"wind":{"speed":4.6,"deg":90},"sys":{"country":"GB"},"rain":null,"snow":null,"clouds":{"all":90},"weather":[{"id":701,"main":"Mist","description":"mist","icon":"50d"},{"id":300,"main":"Drizzle","description":"light intensity drizzle","icon":"09d"}]}]}
        '''
        mocked_post.return_value = MagicMock(status_code=200,
                                             json=lambda: loads(text))

        c = Client()
        response = c.get('/theweather/search/', {'q': 'London'})
        assert response.status_code == 200
        response_json = json.loads(response.content)
        assert isinstance(response_json['weather_info'], list)
        assert len(response_json['weather_info']) > 0
        assert response_json['weather_info'][0].get('location') == "London, GB (-0.1258, 51.5085)"

    @patch('pyowm.commons.http_client.requests.get')
    def test_query_by_lonlat(self, mocked_post):
        text = '''
            {"coord":{"lon":139.01,"lat":35.02},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01n"}],"base":"stations","main":{"temp":285.514,"pressure":1013.75,"humidity":100,"temp_min":285.514,"temp_max":285.514,"sea_level":1023.22,"grnd_level":1013.75},"wind":{"speed":5.52,"deg":311},"clouds":{"all":0},"dt":1485792967,"sys":{"message":0.0025,"country":"JP","sunrise":1485726240,"sunset":1485763863},"id":1907296,"name":"Tawarano","cod":200}
        '''
        mocked_post.return_value = MagicMock(status_code=200,
                                             json=lambda: loads(text))

        c = Client()
        response = c.get('/theweather/search/', {'q': '139,35'})
        assert response.status_code == 200
        response_json = json.loads(response.content)
        assert isinstance(response_json['weather_info'], list)
        assert len(response_json['weather_info']) > 0
        assert response_json['weather_info'][0].get('location') == "Tawarano, JP (139.01, 35.02)"
