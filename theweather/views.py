import logging

from django.conf import settings
from pyowm import OWM
from pyowm.exceptions.api_response_error import NotFoundError
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_404_NOT_FOUND
from rest_framework.views import APIView

from geo import geo


class InvalidCoordinates(Exception):
    pass


class SearchView(APIView):
    """
    Returns the weather for a location. It can be a city id, a lon,lat value or a simple string with a city name or
    part of it.
    """

    def _coords(self, q):
        """
        Parse and validate a string into longitude and latitude values
        :param q: str with a possible pair of coordinates in the form of lon,lat
        :return: a set with two float values, lon and lat. If it's not possible to parse two values, returns None
        """
        result = geo.parse_position(q)
        if result:
            lon, lat = result
            if not -180 <= lon <= 180 or not -90 <= lat <= 90:
                raise InvalidCoordinates()
            else:
                return lon, lat

    def get(self, request, *args, **kwargs):
        try:
            weather_info = []
            q = request.GET.get('q')
            if not q:
                return Response(data='Please enter a location', status=HTTP_400_BAD_REQUEST)
            else:
                owm = OWM(settings.OWM_API_KEY, config_module='TheWeather.configuration25')
                if q.isdigit():
                    forecasts = [owm.weather_at_id(int(q)), ]
                elif ',' in q and (lonlat := self._coords(q)):
                    lon, lat = lonlat
                    forecasts = [owm.weather_at_coords(lat, lon), ]
                else:
                    forecasts = owm.weather_at_places(q, searchtype='like', limit=3)

            for forecast in forecasts:
                w = forecast.get_weather()
                location = forecast.get_location()
                weather_info.append({
                    'location': '{name}, {country} ({lon}, {lat})'.format(
                        name=location.get_name(),
                        country=location.get_country(),
                        lon=location.get_lon(),
                        lat=location.get_lat()
                    ),
                    'detailed_status': w.get_detailed_status().capitalize(),
                    'weather_icon_url': w.get_weather_icon_url(),
                    'temp_celsius': w.get_temperature(unit='celsius')['temp'],
                    'temp_fahrenheit': w.get_temperature(unit='fahrenheit')['temp'],
                    'temp_kelvin': w.get_temperature(unit='kelvin')['temp'],
                })
            return Response({'weather_info': weather_info})
        except NotFoundError:
            msg = 'The searched item was not found, please try a different one'
            return Response(data=msg)
        except InvalidCoordinates:
            msg = 'Please use coordinates in the format of lon,lat.' + \
                  'The valid range of latitude in degrees is -90 and +90 for the southern and northern hemisphere' \
                  ' respectively. Longitude is in the range -180 and +180 specifying coordinates west and east of the' \
                  ' Prime Meridian, respectively.'
            return Response(data=msg)
        except Exception:
            logging.exception(msg='Error on search')
            return Response(data='An error has ocurred, please try again.', status=HTTP_500_INTERNAL_SERVER_ERROR)
