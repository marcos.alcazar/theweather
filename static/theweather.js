$(function (){
  $("#search-form").submit(function(e){
    e.preventDefault();
    $.get('/theweather/search/', $(this).serialize(), function(response){
      if (response['weather_info'].length == 0) {
        $("#results").html('No locations were found, please try again with a different one.')
      } else {
        var tmpl = $.templates("#weather_info");
        $("#results").html(tmpl.render(response['weather_info']));
      }
    }).fail(function(response) {
      $("#results").html(response.responseJSON);
    })
  })
});